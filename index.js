console.log("Hello World");

let firstName = "John";
let lastName = "Smith";
let age = 30;
let hobbies = ["Biking", "Mountain Climbing", "Swimming"];
let workAddress = {
	houseNumber: 32,
	street: "Washington",
	city: "Lincoln",
	state: "Nebraska"
}

console.log ("First Name: John");
console.log ("Last Name: Smith");
console.log ("Age: 30");
console.log ("Hobbies:");
console.log (hobbies);
console.log ("Work Address:");
console.log (workAddress);

function printUserInfo (){
	console.log(firstName + " " + lastName + " is " + age + " years of age.");
	console.log("This was printed inside of the function");
	console.log(hobbies);
	console.log("This was printed inside of the function");
	console.log(workAddress);
}
printUserInfo();

